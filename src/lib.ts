type Parsed = {
	year: number
	month: number
	day: number
	hour: number
	min: number
	ms: number
	isDayBfr: (p: Parsed) => boolean
}

function parseLocal(str: string): Parsed {
	const [date, time] = / à /.test(str) ? str.split(" à ") : str.split(" ")
	const [day, m, year] = date!.split("/").map(s => Number(s)) as [number, number, number]
	const [hour, min, ms] = time!.split(":").map(s => Number(s)) as [number, number, number]
	return {
		year,
		month: m - 1,
		day,
		hour,
		min,
		ms,
		isDayBfr({ year, month, day }: Parsed): boolean {
			if (this.year < year) return true
			else if (this.month < month) return true
			else if (this.day < day) return true
			else return false
		},
	}
}

function getOffset(date: Date, timeZone: string): number {
	// const localeStr = date.toLocaleString("fr-FR")
	const localeStr = new Intl.DateTimeFormat("fr-FR", { dateStyle: "short", timeStyle: "short" }).format(date)
	const locale = parseLocal(localeStr)
	// const tzStr = date.toLocaleString("fr-FR", { timeZone})
	const tzStr = new Intl.DateTimeFormat("fr-FR", { dateStyle: "short", timeStyle: "short", timeZone }).format(date)
	const tz = parseLocal(tzStr)
	if (tz.isDayBfr(locale)) locale.hour += 24
	else if (locale.isDayBfr(tz)) tz.hour += 24
	return (tz.hour - locale.hour) * 60 + tz.min - locale.min
}

export function getTzDate(d: Date, timeZone: string, reverse?: boolean): Date {
	const offset = getOffset(d, timeZone)
	const r = new Date(d)
	r.setUTCMinutes(reverse ? d.getMinutes() + offset : d.getMinutes() - offset)
	return r
}

export function tzDateFactory({
	timeZone,
	reverse,
}: {
	timeZone: string
	reverse?: boolean
}): (...args: unknown[]) => Date {
	return function (...args: unknown[]): Date {
		return getTzDate(new Date(...(args as [])), timeZone, reverse)
	}
}

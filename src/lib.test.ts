import { expect, test } from "vitest"
import { getTzDate } from "./lib"

test.each(["America/Los_Angeles", "Europe/Paris", "UTC"])(
	"simple tests (no hours changes) between current timezone and %s",
	timeZone => {
		const myBirthday = new Date(1987, 0, 23, 5, 37)
		const la = getTzDate(myBirthday, timeZone)
		expect(la.toLocaleString("fr-FR", { timeZone })).equal(myBirthday.toLocaleString("fr-FR"))
	},
)

test.each([
	["2023-10-29T02:00:00.000Z"],
	["2023-10-29T03:00:00.000Z"],
	["2023-10-29T04:00:00.000Z"],
	["2023-10-29T05:00:00.000Z"],
	["2023-10-29T06:00:00.000Z"],
	["2023-10-29T07:00:00.000Z"],
])("test current timeZonw with Europe Paris during winter France hour change 2023 %s: ", (dateStr: string) => {
	const timeZone = "Europe/Paris"
	const date = new Date(dateStr)
	const paris = getTzDate(date, timeZone)
	expect(paris.toLocaleString("fr-FR", { timeZone })).equal(date.toLocaleString("fr-FR"))
})

test.each([
	["2023-03-26T02:00:00.000Z"],
	["2023-03-26T03:00:00.000Z"],
	["2023-03-26T04:00:00.000Z"],
	["2023-03-26T05:00:00.000Z"],
	["2023-03-26T06:00:00.000Z"],
	["2023-03-26T07:00:00.000Z"],
])("test current timeZonw with Europe Paris during summer France hour change 2023 %s: ", (dateStr: string) => {
	const timeZone = "Europe/Paris"
	const date = new Date(dateStr)
	const paris = getTzDate(date, timeZone)
	expect(paris.toLocaleString("fr-FR", { timeZone })).equal(date.toLocaleString("fr-FR"))
})
